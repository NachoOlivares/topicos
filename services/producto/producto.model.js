var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Producto', new Schema({ 
    codigo: String, 
    nombre: {type: String, index: true, unique: true}, 
    precio: Number,
    minStock: String,
    proveedor: { type: mongoose.Schema.Types.ObjectId, ref: 'Proveedor'},
    fecha: { type: Date, default: Date.now },
    activo: { type: Boolean, default: true }, 
}));