/************************************************* Requireds *******************************************************/
var express = require('express');
var mongoose = require('mongoose');
var bodyParser  = require('body-parser');
var morgan = require('morgan');
var app = express();
var configuracion = require('./configuracion/config');
/************************************************* Servicios *******************************************************/

/***************  productos  ******************/
var productos = require('./services/producto/producto.routes');
app.use('/api/producto', productos); 
/*************** Fin productos  ******************/

/***************  proveedor  ******************/
var proveedor = require('./services/proveedor/proveedor.routes');
app.use('/api/proveedor', proveedor); 
/*************** Fin productos  ******************/

/************************************************* Configuracion del Servidor *******************************************************/
var puerto = process.env.PORT || 3000; 
mongoose.connect(configuracion.database); // Conecctado de la BD
app.set('superSecret', configuracion.secret); // Variable secreta
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(morgan('dev')); // Para que aparezcan en la Consola los logs de peticiones
app.use(express.static('public')); // Para exponer una carpeta de archivos a las llamadas get de la ruta.

/************************************************* Inicio del Servidor *******************************************************/
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(puerto, function () {
  console.log(new Date().toString() + "-Servidor Corriendo en el puerto:"+puerto);
});